# Web greeter

A modern, visually appealing greeter for LightDM.
  Web Greeter utilizes themes built with HTML/CSS/JavaScript for it's login screen. Web Greeter
  themes provide modern, visually appealing, and feature-rich login screens. Two themes are
  included by default. There is also a growing number of 3rd-Party themes available online.